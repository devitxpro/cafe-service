package uz.devit.x;

public class Main {
    public static void main(String[] args) {

        User user = new User();
        user.setFirstname("Sardor");
        user.setLastname("Xushvaqtov");
        user.setLogin("sardorxushvaqtov34@gmail.com");
        user.setPassword("12345");
        user.setServicePercent(15);

        Food osh = new Food();
        Food choy = new Food();
        Food salat = new Food();
        osh.setName("To'y oshi");
        osh.setPrise(36000);
        osh.setCategory(Food.Category.SECOND);

        choy.setName("Choy");
        choy.setPrise(2000);
        choy.setCategory(Food.Category.DRINKS);

        salat.setName("Salat");
        salat.setPrise(9000);
        salat.setCategory(Food.Category.SALADS);

        Table table = new Table();
        table.setName("1");
        table.setPeopleCount(2);
        table.setLocation("Zal");

        Order order = new Order();
        order.setUser(user);
        order.setTable(table);
        // Foodni o'rniga OrderItemdan obeykt yaratiladi,Array yaratiladi, Orderni ichiga OrderItemlar saqlanadi.
        // !Har bir ovqatga ketadigan maxsulotlar ro'yxati va xarajati.

        OrderItem oI1 = new OrderItem();
        oI1.setFood(osh);
        oI1.setNumber(2);

        OrderItem oI2 = new OrderItem();
        oI2.setFood(choy);
        oI2.setNumber(1);

        OrderItem oI3 = new OrderItem();
        oI3.setFood(salat);
        oI3.setNumber(2);

        OrderItem[] array = new OrderItem[3];
        array[0] = oI1;
        array[1] = oI2;
        array[2] = oI3;
        order.setOrderItemsList(array);
        order.printOrder();
        
    }
}
