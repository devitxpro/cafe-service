package uz.devit.x;

public class User {

    private String firstname;
    private String lastname;
    private String login;
    private String password;
    private int servicePercent;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;

    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getServicePercent() {
        return servicePercent;
    }

    public void setServicePercent(int servicePercent) {
        this.servicePercent = servicePercent;
    }

    @Override
    public String toString() {
        return firstname + " " + lastname;
    }
}
