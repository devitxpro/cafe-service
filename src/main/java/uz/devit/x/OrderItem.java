package uz.devit.x;

public class OrderItem {
    // Ovqat , zakazlar soni,
    private Food food;

    private int number;


    public void setFood(Food food) {
        this.food = food;
    }

    public Food getFood() {
        return food;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return food.toString() + " soni: " + number;
    }
}
