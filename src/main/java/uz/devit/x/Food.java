package uz.devit.x;

public class Food {

    public enum Category {FIRST, SECOND, DRINKS, SALADS;}

    private String name;

    private double prise;

    private Category category;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrise() {
        return prise;
    }

    public void setPrise(double prise) {
        this.prise = prise;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return category + " nomi: " + name + " narxi: " + prise;
    }
}
