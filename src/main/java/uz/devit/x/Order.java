package uz.devit.x;

public class Order {

    private Table table;

    private User user;

    private OrderItem[] orderItemsList;

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderItem[] getOrderItemsList() {
        return orderItemsList;
    }

    public void setOrderItemsList(OrderItem[] orderItemsList) {
        this.orderItemsList = orderItemsList;
    }

    public void printOrder() {
        System.out.println(user);
        System.out.println(table);
        double totalPrice = 0;
        for (OrderItem orderItem : orderItemsList) {
            System.out.println(orderItem);
            totalPrice += orderItem.getFood().getPrise() * orderItem.getNumber();
        }
        double percent = totalPrice * user.getServicePercent() / 100;
        double toPay = percent + totalPrice;
        System.out.println("Jami hisob: " + totalPrice);
        System.out.println("Xizmat haqi: " + percent);
        System.out.println("To'lov: " + toPay);
    }

}
